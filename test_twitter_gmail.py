import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

class FirstTweet(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://twitter.com/")
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()

    def test_tweet(self):
        # 1 Click on button Sign in
        button_signup01 = self.driver.find_element_by_partial_link_text("Sign Up")
        button_signup01.click()

        # 2 Type name
        type_name = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='name']")))
        type_name.send_keys("Adigen")

        # 3 Choose Use email instead
        mail2phone = self.driver.find_element_by_xpath(("//div[@role='button'][@data-focusable='true']"))
        self.driver.execute_script("arguments[0].click();", mail2phone)

        # 4 Type email
        type_mail = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
        type_mail.send_keys("Mals1961@armyspy.com")
        # type_mail.send_keys(Keys.TAB + Keys.TAB)

        # 5 Click Next
        # button_next = self.driver.find_element_by_xpath(("//div[@role='button'][@data-focusable='true']"))
        button_next01 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next01.click()


        # 6 Click button Sign Up
        # button_sighup = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-19h5ruw.r-1jayybb.r-17bavie.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_signup02 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-76zvg2.r-1awozwy.r-jwli3a.r-6koalj.r-18u37iz.r-16y2uox.r-1qd0xha.r-1b6yd1w.r-vw2c0b.r-1777fci.r-eljoum.r-dnmrzs.r-bcqeeo.r-q4m81j.r-qvutc0")))

        button_signup02.click()
        time.sleep(5)
        self.driver.save_screenshot("screenshot.png")

        # 7 Go to mail page
        

        # 8 Check mail for code

        # 9 Copy code

        # 10 Go to twitter page

        # 11 Paste code -> Next
        type_code = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name = 'verfication_code']")))
        type_code.send_keys("000000")

        # 12 Click Next
        button_next02 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next02.click()



if __name__ == '__main__':
    unittest.main(verbosity=2)
