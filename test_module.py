import time
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
twitter_name = ""
twitter_email = ""
twitter_password = "000000000"

class MailLogin(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://www.google.com/")
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()

    def test_mail(self):

        # 4 Go to page https://www.disposablemail.com/
        twitter_windows = self.driver.current_window_handle
        self.driver.execute_script("window.open('https://www.disposablemail.com/')")
        WebDriverWait(self.driver, 10).until(EC.number_of_windows_to_be(2))
        mail_windows = self.driver.window_handles
        self.driver.switch_to.window(mail_windows[1])
        time.sleep(10)

        """# 5 Click button COPY
        copy_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, "copy-button")))
        copy_button.click()


        # 6 Go to test registration
        button_next03 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, "identifierNext")))
        button_next03.click()

        # 11 Type password
        type_pass = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[type = 'password']")))
        type_pass.send_keys("FunFunM@1l")

        # 12 Click Next
        button_next04 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, "passwordNext")))
        button_next04.click()


        # 8 Check mail for code
        letter_verify = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//span[@email='verify@twitter.com'][@name='verify']")))
        #letter_verify.click()


        # 9 Copy code

        # 10 Go to twitter page

        # 11 Paste code -> Next


        # 1 Click on button Sign in
        
        

        # 12 Click Next
        self.driver.switch_to.window(mail_windows[0])
        searcher = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.NAME, "q")))
        searcher.click()
        searcher.send_keys(Keys.CONTROL, 'v')
        searcher.send_keys(twitter_password)

        # 12 Open mail with code
        # letter = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//td[@class = 'h1.black']")))
        mail_elements = self.driver.find_elements_by_tag_name("p")
        text_in_elements = map(lambda x: x.text, mail_elements)
        text_code = list(filter(lambda y: "is your Twitter verification code" in y, text_in_elements))[0]
        code = text_code[:6]
        print(code)

        letter = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//li[@class = 'copy']")))
        letter.click()"""

        WebDriverWait(self.driver, 30).until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.email-flex")))
        elements_in_mail = self.driver.find_elements_by_css_selector("div.subject")
        text_in_elements = map(lambda x: x.text, elements_in_mail)
        text_code = list(filter(lambda y: "is your Twitter verification code" in y, text_in_elements))[0]
        code = text_code[:6]
        self.driver.switch_to.window(mail_windows[0])
        searcher = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.NAME, "q")))
        searcher.click()
        searcher.send_keys(code)



        # WebDriverWait(self.driver, 20).until_not(EC.visibility_of_element_located((By.XPATH, "//div[@class = 'loader']")))

        # 13 Copy code

        # 14 Go to twitter page

        # self.driver.switch_to.window(mail_windows[0])
        # searcher = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.NAME, "q")))
        # searcher.click()
        # searcher.send_keys(code)




        time.sleep(5)
        self.driver.save_screenshot("screenshot2.png")





if __name__ == '__main__':
    unittest.main(verbosity=2)
