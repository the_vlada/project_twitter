import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

twitter_name = "pantofel"
twitter_password = "p4ssW0rd"


class FirstTweet(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://twitter.com/")
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()

    def test_tweet(self):
        # 1 Click button Sign Up on start page
        button_signup01 = self.driver.find_element_by_partial_link_text("Sign Up")
        button_signup01.click()

        # 2 Type name
        type_name = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='name']")))
        type_name.send_keys(twitter_name)

        # 3 Choose Use email instead
        mail2phone = self.driver.find_element_by_xpath(("//div[@role='button'][@data-focusable='true']"))
        self.driver.execute_script("arguments[0].click();", mail2phone)

        # 4 Go to page https://www.disposablemail.com
        twitter_windows = self.driver.current_window_handle
        self.driver.execute_script("window.open('https://www.disposablemail.com')")
        WebDriverWait(self.driver, 10).until(EC.number_of_windows_to_be(2))
        mail_windows = self.driver.window_handles
        self.driver.switch_to.window(mail_windows[1])
        time.sleep(3)

        # 5 Click button COPY
        copy_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//li[@class = 'copy']")))
        time.sleep(3)
        copy_button.click()

        # 6 Go to twitter`s registration
        self.driver.switch_to.window(mail_windows[0])

        # 7 Paste email
        paste_mail = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
        paste_mail.send_keys(Keys.CONTROL, 'v')

        # 8 Click button Next
        button_next01 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next01.click()

        # 9 Customize your experience
        checkbox = self.driver.find_element_by_css_selector("input.r-1p0dtai.r-1ei5mc7.r-1pi2tsx.r-1d2f490.r-crgep1.r-orgf3d.r-t60dpp.r-u8s1d.r-zchlnj.r-ipm5af.r-13qz1uu")
        self.driver.execute_script("arguments[0].click();", checkbox)

        # 9 Click button Next
        button_next02 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next02.click()

        # 10 Click button Sign in
        button_signup02 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-19h5ruw.r-1jayybb.r-17bavie.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_signup02.click()

        # 11 Go to mail page
        self.driver.switch_to.window(mail_windows[1])

        # 12 Get code from mail
        WebDriverWait(self.driver, 90).until_not(EC.visibility_of_element_located((By.ID, "emptyMailbox")))
        elements_in_mail = self.driver.find_elements_by_xpath("//div[@class = 'subject']")
        text_in_elements = map(lambda x: x.text, elements_in_mail)
        text_code = list(filter(lambda y: "is your Twitter verification code" in y, text_in_elements))[0]
        code = text_code[:6]

        # 13 Go to twitter page
        self.driver.switch_to.window(mail_windows[0])

        # 14 Paste code
        type_code = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name = 'verfication_code']")))
        type_code.send_keys(code)

        # 15 Click button Next
        button_next03 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next03.click()

        # 16 Create password
        type_code = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name = 'password']")))
        type_code.send_keys(twitter_password)

        # 17 Click button Next
        button_next04 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next04.click()

        # 18 Profile picture -> Click button Skip
        button_skip01 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_skip01.click()

        # 19 Describe yourself -> Click button Skip
        button_skip02 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_skip02.click()

        # 20 Find friends -> Click button Not now
        # button_not_now01 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By., "")))
        # button__not_now01.click()

        # 21 Interests -> Click button Skip
        button_not_now02 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_not_now02.click()

        # 22 Suggestions -> Click button Next
        button_next04 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.css-18t94o4.css-1dbjc4n.r-urgr8i.r-42olwf.r-sdzlij.r-1phboty.r-rs99b7.r-1w2pmg.r-1vsu8ta.r-aj3cln.r-1fneopy.r-o7ynqc.r-6416eg.r-lrvibr")))
        button_next04.click()

        # 23 Notifications -> Click button Skip
        # button_skip03 = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By., "")))
        # button_skip03.click()

        # 24 Click button Tweet
        button_tweet = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, "global-new-tweet-button")))
        button_tweet.click()

        # 25 Choose first tweet
        first_tweet = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.tweet-box.rich-editor")))
        first_tweet.send_keys("Hello!")

        # 26 Click button Send
        button_send = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.SendTweetsButton.EdgeButton.EdgeButton--primary.EdgeButton--medium.js-send-tweets")))
        button_send.click()

        # 27 Click to see first tweet
        button_see = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.stream - item.js - new - items - bar - container.new - tweets - bar - visible")))
        button_see.click()



        time.sleep(5)
        self.driver.save_screenshot("screenshot.png")


if __name__ == '__main__':
    unittest.main(verbosity=2)
